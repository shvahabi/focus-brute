# Focus Brute

A simple restful client implementing [brute force search](https://en.wikipedia.org/wiki/Brute-force_search) to find possible key-value combinations which successfully create/update selected endpoint of undocumented [Focus](https://www.focussoftnet.com/) API.
